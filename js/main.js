'use strict'

/**
 * Теорія:
 * 1. Опишіть, як можна створити новий HTML тег на сторінці.
 * Виведення нового тегу на сторінку виконується у дві дії:
 * - власне створення елементу в оперативній пам'яті (метод document.createElement(...))
 * - вставка тегу на сторінку element.append/prepend/before/after(...).
 *
 * 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
 * перший параметр insertAdjacentHTML - position - визначає позицію для елемента що буде вставлений відносно елмента,
 * для якого викликано метод.
 * Можливі значення: beforebegin, afterbegin, beforeend, afterend
 * Приклад:
 *
 * тут буде beforebegin
 * <element>
 *   тут буде afterbegin
 *   ...
 *   ...
 *   ...
 *   тут буде beforeend
 * </element>
 * тут буде afterend
 *
 * 3. Як можна видалити елемент зі сторінки?
 * Викликати метод remove() для елемента.
 *
 * Завдання
 * Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку.
 * Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
 *
 * Технічні вимоги:
 * Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент,
 * до якого буде прикріплений список (по дефолту має бути document.body.
 * кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
 * Приклади масивів, які можна виводити на екран:
 *
 * ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
 * ["1", "2", "3", "sea", "user", 23];
 * Можна взяти будь-який інший масив.
 * Необов'язкове завдання підвищеної складності:
 * Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список. Приклад такого масиву:
 *
 * ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
 * Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
 * Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки. *
 */

let arr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let arr2 = ["1", "2", "3", "sea", "user", 23];
let arr3 = ["Kharkiv", "Kiev", ["Borispol", ['aaa', 'bbb'], "Irpin"], "Odessa", "Lviv", "Dnieper"];

let nodeForList = document.querySelector('.test-node');

function createList(arr, node = document.body) {
    function rec(arr) {
        return arr.map(el => {
            return (!Array.isArray(el)) ? `<li>${el}</li>` : `<ul>${rec(el)}</ul>`;
        }).join('');
    }
    return node.innerHTML = `<ul>${rec(arr)}</ul>`;
}

createList(arr3, nodeForList);

let count = 5;
let timer = document.createElement("div");
timer.innerText = `${count}`;
document.body.append(timer);

setInterval(() => {
    if (count > 0) {
        count--;
        timer.innerText = `${count}`;
    }
}, 1000);

setTimeout(() => {
    nodeForList.remove();
    timer.remove();
}, 5000);







